package com.kuding.text;

import com.kuding.pojos.ExceptionNotice;

@FunctionalInterface
public interface ExceptionNoticeResolver extends NoticeTextResolver<ExceptionNotice> {

}
